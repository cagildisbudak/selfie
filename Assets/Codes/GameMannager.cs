using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameMannager : MonoBehaviour
{
  
    private Animator Ani;
    public Image right;
    public Image left;
    public Image down;
    public Image up;

    public Image Pose;
    public Image PoseDown;
    public Image PoseUp;
    public Image PoseLeft;

    

    int i = 0;
    void Start()
    {
        Ani =GetComponent<Animator>();
        right.enabled = false;
        left.enabled = false;
        down.enabled = false;
        up.enabled = false;

        Pose.enabled = false;
        PoseDown.enabled=false;
        PoseUp.enabled=false;
        PoseLeft.enabled=false;
       
       
    }

    // Update is called once per frame
    void Update()
    {
       

        Touch parmak = Input.GetTouch(0);
      

            if (Input.touchCount > 0)
        {
          
               
                if (parmak.deltaPosition.x > 50.0f && TouchPhase.Ended == parmak.phase)
                {
                    
                        Ani.SetTrigger("Pose_0");
                        Pose.enabled = true; 
                        right.color = new Color(0, 255, 10, 255);
                i++;
  
                }
                else if (parmak.deltaPosition.y > 50.0f && TouchPhase.Ended == parmak.phase)
                {
                   
                        Ani.SetTrigger("Pose_1");
                        up.color = new Color(0, 255, 10, 255);
                        PoseUp.enabled = true;
                i++;
            }
                else if (parmak.deltaPosition.y < -50.0f && TouchPhase.Ended == parmak.phase)
                {
                   
                        Ani.SetTrigger("Pose_2");
                        
                      
                        down.color = new Color(0, 255, 10, 255);
                       PoseDown.enabled = true;
                i++;

            }
                else if (parmak.deltaPosition.x < -50.0f && TouchPhase.Ended == parmak.phase)
                {
                        
                        Ani.SetTrigger("Pose_3");
                      left.color = new Color(0, 255, 10, 255);
                      PoseLeft.enabled = true;
                i++;
            }
            if (i == 4) 
            {
                SceneManager.LoadScene("Win");
            }
            
        }
      
    }

}
